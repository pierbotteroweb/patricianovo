var linksMenuHoriz = require("./linksMenuHoriz.hbs");
var linksSideMenu = require("./linksSideMenu.hbs");

var links = {
   "links":[
        {
          "nome": "HOME",
          "url":"#"
        },
        {
          "nome": "SOBRE A TERAPEUTA",
          "url":"#"
        },
        {
          "nome": "O QUE É ROLFING",
          "url":"#"
        },
        {
          "nome": "A CLÍNICA",
          "url":"#"
        },
        {
          "nome": "DEPOIMENTOS",
          "url":"#"
        },
        {
          "nome": "CONTATO",
          "url":"#"
        }
    ]
 }


 function createHTML(dadosEmJSON){

  var navbar = document.getElementById("navbar");
  var sidemenu = document.getElementById("sidemenu");
  navbar.innerHTML = linksMenuHoriz(dadosEmJSON);
  sidemenu.innerHTML = linksSideMenu(dadosEmJSON);
}

createHTML(links)