
const path = require("path");

module.exports = {
  entry: {
    main:"./js/main.js",
    slide:"./js/slide.js",
    links:"./js/links.js"
  },
  output: {
    path: path.resolve(__dirname, "./dist/js"),
    filename: "[name]-bundled.js"
  },
  mode:'development',
  module: {
    rules: [
      {
        test: /\.hbs$/,
        use: [{
          loader: "handlebars-loader",
          options: {helperDirs: path.resolve(__dirname, "./dist/js/helpers")}
        }]
      }
    ]
  }
};