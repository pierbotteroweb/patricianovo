# layoutmodelo
Gulp, Webpack e Handlebars


Sistema de automação em Gulp para projetos com uma ou mais paginas e montagem de cada pagina usando o mesmo header e footer. 
Pré processador de CSS SASS.
Browser-sync
Webpack configurado para utilização de templates handlebars.

Criando uma nova pagina:

Para criar uma nova pagina, é necessário adicionar o nome da pagina no array 
'pages' em gulpPages.js

Criando um novo scss:

Na criação de um novo modulo de estilização .css na pasta csstemp que são gerados automaticamente pelo script do gulp, o novo modulo deve ser importado manualmente para o style.css na mesma pasta.